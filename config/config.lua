local request = require("http.request")

-- basic configs
exodus {
   port = 1080,
   mode = "Rule",
   log = "info",
   allowLan = true,
}

-- proxies
proxy "vmess" {
   type = "vmess",
   server = "jp2.vmess.com",
   port = 443,
   uuid = "uuid",
   alterId = "alterId",
   cipher = "auto",
   tls = "true",
}

proxy "socks1" {
   type = "socks5",
   server = "1.1.1.1",
   port = 443,
   username = "username",
   password = "password"
}

proxy "http" {
   type = "http",
   server = "server",
   port = 443,
   username = "username",
   password = "password",
}

-- generate proxies in loops
local servers = { "jp1", "cn2", "us3" }
for i, server in pairs(servers) do
   proxy ("ss" .. i ) {
      type = "ss",
      server = server,
      port = 443,
      password = "asd",
      cipher = "chacha20",
   }
end

-- proxy groups for gui
proxyGroup "auto" {
   type = "url-test",
   proxies = { "ss1", "vmess", "socks1", "http" },
   url = "http://www.gstatic.com/generate_204",
   interval = 300,
}

proxyGroup "load-balance" {
   type = "load-balance",
   proxies = { "ss1", "vmess", "socks1", "http" },
   url = "http://www.gstatic.com/generate_204",
   interval = 300,
}

proxyGroup "select" {
   type = "select",
   proxies = { "ss1", "vmess", "socks1", "http", "auto" },
   url = "http://www.gstatic.com/generate_204",
}

-- rules
rule {
   type = "DOMAIN-SUFFIX",
   targets = { "google.com", "youtube.com" },
   policy = "auto",
}

rule {
   type = "DOMAIN",
   target = "twitter.com",
   policy = "auto",
}

rule {
   type = "GEOIP",
   target = "CN",
   policy = "DIRECT",
}

rules {
   "DOMAIN-SUFFIX,ad.com,REJECT",
   "IP-CIDR,127.0.0.0/8,DIRECT",
   "SRC-IP-CIDR,192.168.1.201/32,DIRECT",
   "GEOIP,CN,DIRECT",
   "DST-PORT,80,DIRECT",
   "SRC-PORT,7777,DIRECT",
   "MATCH,auto",
}

-- generate rules from remote list
local h, s = request.new_from_uri("https://raw.githubusercontent.com/Hackl0us/SS-Rule-Snippet/master/Rulesets/Basic/CN.list"):go()
local body = s:get_body_as_string()
for line in body:gmatch("[^\r\n]+") do
   -- CN list goes direct
   rules {
      line .. ",DIRECT"
   }
end
