use crate::protocol::*;
use async_trait::async_trait;
use exodus;
use exodus::{forward_stream, AsyncStream, TargetAddress};
use serde::{Deserialize, Serialize};
use std::net::{IpAddr, SocketAddr};
use std::pin::Pin;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Config {
    pub server: String,
    pub port: u16,
}

pub struct Redirector {
    remote: SocketAddr,
}

impl Redirector {
    pub fn new(config: &Config) -> Result<Redirector, Error> {
        // TODO: Support domain
        let ip = config.server.parse::<IpAddr>()?;
        Ok(Redirector {
            remote: SocketAddr::new(ip, config.port),
        })
    }
}

impl Redirector {
    async fn handle(&self, conn: &mut TcpStream, addr: &TargetAddress) -> Result<(), Error> {
        negotiate(conn).await?;
        write_request(conn, addr).await?;
        read_reply(conn).await?;
        Ok(())
    }
}

#[async_trait]
impl exodus::Redirector for Redirector {
    async fn redirect(&self, addr: &TargetAddress, src: Pin<&mut dyn AsyncStream>) {
        match TcpStream::connect(self.remote).await {
            Ok(mut conn) => {
                if let Err(e) = self.handle(&mut conn, addr).await {
                    eprintln!("handle error: {:?}", e);
                    return;
                }
                futures::pin_mut!(conn);
                forward_stream(src, conn).await;
            }
            Err(e) => eprintln!("handle error: {:?}", e),
        }
    }
}

async fn negotiate(conn: &mut TcpStream) -> Result<(), Error> {
    let req = [VER_SOCKS5, 1, NO_AUTH];
    conn.write_all(&req).await?;
    let mut ver_method = [0u8; 2];
    conn.read_exact(&mut ver_method).await?;
    let [ver, method] = ver_method;
    if ver != VER_SOCKS5 {
        return Err(Error::UnsupportVersion);
    }
    if method != NO_AUTH {
        return Err(Error::UnsupportAuthMethod);
    }
    Ok(())
}

/// Once the method-dependent subnegotiation has completed, the client
/// sends the request details.  If the negotiated method includes
/// encapsulation for purposes of integrity checking and/or
/// confidentiality, these requests MUST be encapsulated in the method-
/// dependent encapsulation.
///
/// The SOCKS request is formed as follows:
///
///      +----+-----+-------+------+----------+----------+
///      |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
///      +----+-----+-------+------+----------+----------+
///      | 1  |  1  | X'00' |  1   | Variable |    2     |
///      +----+-----+-------+------+----------+----------+
///
///   Where:
///
///        o  VER      protocol version: X'05'
///        o  CMD
///           o  CONNECT X'01'
///           o  BIND X'02'
///           o  UDP ASSOCIATE X'03'
///        o  RSV      RESERVED
///        o  ATYP     address type of following address
///           o  IP V4 address: X'01'
///           o  DOMAINNAME: X'03'
///           o  IP V6 address: X'04'
///        o  DST.ADDR desired destination address
///        o  DST.PORT desired destination port in network octet
///           order
///
/// The SOCKS server will typically evaluate the request based on source
/// and destination addresses, and return one or more reply messages, as
/// appropriate for the request type.
async fn write_request(conn: &mut TcpStream, addr: &TargetAddress) -> Result<(), Error> {
    let mut buf = Vec::new();
    buf.push(VER_SOCKS5);
    buf.push(CMD_CONNECT);
    buf.push(0); // RESERVED
    write_address(&mut buf, addr)?;
    conn.write_all(&buf).await?;
    Ok(())
}

/// The SOCKS request information is sent by the client as soon as it has
/// established a connection to the SOCKS server, and completed the
/// authentication negotiations.  The server evaluates the request, and
/// returns a reply formed as follows:
///
///      +----+-----+-------+------+----------+----------+
///      |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
///      +----+-----+-------+------+----------+----------+
///      | 1  |  1  | X'00' |  1   | Variable |    2     |
///      +----+-----+-------+------+----------+----------+
///
///   Where:
///
///        o  VER    protocol version: X'05'
///        o  REP    Reply field:
///           o  X'00' succeeded
///           o  X'01' general SOCKS server failure
///           o  X'02' connection not allowed by ruleset
///           o  X'03' Network unreachable
///           o  X'04' Host unreachable
///           o  X'05' Connection refused
///           o  X'06' TTL expired
///           o  X'07' Command not supported
///           o  X'08' Address type not supported
///           o  X'09' to X'FF' unassigned
///        o  RSV    RESERVED
///        o  ATYP   address type of following address
///           o  IP V4 address: X'01'
///           o  DOMAINNAME: X'03'
///           o  IP V6 address: X'04'
///        o  BND.ADDR       server bound address
///        o  BND.PORT       server bound port in network octet order
///
/// Fields marked RESERVED (RSV) must be set to X'00'.
async fn read_reply(conn: &mut TcpStream) -> Result<(), Error> {
    let mut res = [0u8; 3];
    conn.read_exact(&mut res).await?;
    read_address(conn).await?; // Dropped the bound address
    let [_ver, rep, _rsv] = res;
    match rep {
        0 => Ok(()),
        _ => Err(Error::ConnectFail),
    }
}
