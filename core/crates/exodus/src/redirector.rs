use crate::stream::AsyncStream;
use async_trait::async_trait;
use futures::future::try_select;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::pin::Pin;
use tokio;

pub enum TargetHost {
    Domain(String),
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}

pub struct TargetAddress {
    pub host: TargetHost,
    pub port: u16,
}

pub async fn forward_stream(lh: Pin<&mut dyn AsyncStream>, rh: Pin<&mut dyn AsyncStream>) {
    let (mut rl, mut wl) = tokio::io::split(lh);
    let (mut rr, mut wr) = tokio::io::split(rh);
    let upstream = tokio::io::copy(&mut rl, &mut wr);
    let downstream = tokio::io::copy(&mut rr, &mut wl);
    match try_select(upstream, downstream).await {
        // TODO: Record r/w speed or dataflow (maybe add a statistic
        // handler in parameters)
        _ => {}
    };
}

#[async_trait]
pub trait Redirector: Send + Sync {
    async fn redirect(&self, addr: &TargetAddress, src: Pin<&mut dyn AsyncStream>);
}
