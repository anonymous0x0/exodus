use crate::dispatcher::Dispatcher;
use async_trait::async_trait;
use std::sync::Arc;

#[async_trait]
pub trait Listener {
    async fn accept(&mut self, dispatcher: Arc<Dispatcher>);
}
