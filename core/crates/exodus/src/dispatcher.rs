use crate::redirector::{Redirector, TargetAddress};
use crate::stream::AsyncStream;
use std::collections::HashMap;
use std::pin::Pin;

pub struct Dispatcher {
    redirectors: HashMap<String, Box<dyn Redirector>>,
}

impl Dispatcher {
    pub fn new(redirectors: HashMap<String, Box<dyn Redirector>>) -> Dispatcher {
        Dispatcher {
            redirectors: redirectors,
        }
    }
    pub fn rule_match(&self, _addr: &TargetAddress) -> String {
        // TODO: implement rule match
        return String::from("DEFAULT");
    }
    pub async fn redirect(&self, addr: &TargetAddress, src: Pin<&mut dyn AsyncStream>) {
        let rule = self.rule_match(addr);
        self.redirectors
            .get(rule.as_str())
            .unwrap()
            .redirect(addr, src)
            .await;
    }
}
