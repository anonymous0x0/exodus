use exodus::*;
use socks5;
use std::collections::HashMap;
use std::fs::File;
use std::string::String;
use std::sync::Arc;
use structopt::StructOpt;
use thiserror;
use tokio;

mod config;

#[derive(StructOpt)]
#[structopt(name = "exodus")]
struct Opts {
    #[structopt(short, long, default_value = "/etc/exodus/config.json")]
    config: String,
}

#[derive(thiserror::Error, Debug)]
enum Error {
    #[error("config error: {0}")]
    ConfigError(String),
    #[error("{0:?}")]
    IOError(#[from] std::io::Error),
    #[error("{0:?}")]
    JSONError(#[from] serde_json::error::Error),
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts = Opts::from_args();
    let f = File::open(&opts.config)?;
    let config = config::from_file(&f)?;
    println!("run exodus with config: {}", opts.config);
    println!("read config: {:#?}", config);

    let mut redirectors: HashMap<String, Box<dyn Redirector>> = HashMap::new();
    for (name, config) in config.proxies {
        match config {
            config::Redirector::Socks5(c) => {
                redirectors.insert(
                    name,
                    Box::new(socks5::redirector::Redirector::new(&c).unwrap()),
                );
            }
            _ => unreachable!(),
        }
    }

    let dispacher = Arc::new(Dispatcher::new(redirectors));
    // TODO: Support multiple listener
    match config.listen {
        config::Listen::Socks5 { addr } => {
            let mut l = socks5::listener::Listener::new(addr).await.unwrap();
            loop {
                l.accept(dispacher.clone()).await;
            }
        }
        _ => {
            return Err(Error::ConfigError(String::from("unsupported listener")));
        }
    };
}
