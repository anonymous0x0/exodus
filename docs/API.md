<!-- -*- mode: apib -*- -->
FORMAT: 1A
HOST: http://polls.apiblueprint.org/

# Exodus Core API

## Data Structures

### Error
- code (number)
- message (string)

### Policy
- type: `url-test` (string)
- name: `auto` (string)
- proxies: `ss1` (array[string])
- current: `ss1` (string)

### Proxy
- type: `ss` (string)
- cipher: `chacha20` (string)
- server: `ss.server.com` (string)
- port: `443` (number)

### Settings
- httpPort: `8080` (number)
- socksPort: `1080` (number)
- port: `1024` (number)
- allowLan: `false` (boolean)
- mode: `rule` (string)
- log: `info` (string)

## Settings [/settings]

### Get Settings [GET]

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (Settings, required)

### Modify Settings [PUT]

+ Request (application/json)
    + Attributes (Settings)

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (Settings, required)

## Policies [/policies]

### List Policies [GET]

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (array[Policy], required)

### Policy Detail [GET /policies/{name}]

+ Parameters
    + name: `select` (string, required) - name of policy

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (array[Policy], required)


### Switch Proxy (only works on select type policy) [PATCH /policies/{name}]

+ Parameters
    + name: `select` (string, required) - name of policy

+ Request (application/json)
    + Attributes
      + name: `ss1` (string, required) - name of proxy

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (Policy, required)

## Proxies [/proxies]

### List Proxies [GET]

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (array[Proxy], required)

### Proxy Detail [GET /proxies/{name}]

+ Parameters
    + name: `ss1` (string, required) - name of proxy

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (Proxy, required)

### Proxy delay [GET /proxies/{name}/delay]

+ Response 200 (application/json)
    + Attributes
      + error (Error, required)
      + data (object, required)
          - delay: `100` (number, required) -- ms
